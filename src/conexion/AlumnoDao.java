package conexion;

import model.Alumno;
import org.hibernate.Session;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Sandra
 */
public class AlumnoDao {
    
    //consulta alumno
    public Alumno obtenerAlumno(int id){
        Session s = HibernateUtil.getCurrentSession();
        s.beginTransaction();
        Alumno result = (Alumno) s.get(Alumno.class, id);
        s.getTransaction().commit();
        s.close();
        return result;
    }
    
    //alta usuario
    public void altaAlumno(Alumno a){
        Session s = HibernateUtil.getCurrentSession();
        s.beginTransaction();
        s.save(a);
        s.getTransaction().commit();
        s.close();
    }
    
    //baja usuario
    public void bajaAlumno(Alumno a){
        Session s = HibernateUtil.getCurrentSession();
        s.beginTransaction();
        s.delete(a);
        s.getTransaction().commit();
        s.close();
    }
}
