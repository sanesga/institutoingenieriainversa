/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import conexion.AlumnoDao;
import conexion.ProfesorDao;
import java.util.Calendar;
import java.util.Scanner;
import model.Alumno;
import model.Nombre;
import model.Profesor;
import model.TipoFuncionario;

public class App {

    public static void main(String[] args) {

        Scanner teclado = new Scanner(System.in);

        int opcion, id;
        AlumnoDao ad = new AlumnoDao();
        ProfesorDao pd= new ProfesorDao();
        Alumno a = null;
        Profesor p = null;
        String nombre, observaciones,ape1,ape2;
        Calendar fechaNac, horaTutoria, fechaRegistro;
        char registrado;
        float sueldo;
        TipoFuncionario tipoFuncionario = null;
        Nombre n;

        do {
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR ALUMNO
                    id = 1;
                    nombre = "Pepe";
                    sueldo = 1200F;
                    registrado = 'S';
                    fechaNac = Calendar.getInstance();
                    fechaNac.set(1980, 02, 10);
                    horaTutoria = Calendar.getInstance();
                    horaTutoria.set(0, 0, 0, 12, 30);
                    fechaRegistro = Calendar.getInstance();
                    observaciones = "Esto son observaciones";

                    a = new Alumno(id, nombre, sueldo, registrado, fechaNac.getTime(), horaTutoria.getTime(), fechaRegistro.getTime(), observaciones);

                    System.out.println("Alumno creado con éxito");
                    break;
                case 2://GUARDAR ALUMNO
                    ad.altaAlumno(a);
                    System.out.println("Alumno guardado con éxito");
                    break;
                case 3://LEER ALUMNO
                    a=ad.obtenerAlumno(1);
                     System.out.println(a);
                    break;
                case 4://ELIMINAR ALUMNO
                    ad.bajaAlumno(a);
                    System.out.println("Alumno eliminado con éxito");
                    break;
                case 5://CREAR PROFESOR
                   n = new Nombre("Juan", "Martínez", "Soria");
                    
                    p = new Profesor(n, tipoFuncionario.INTERINO);
                    System.out.println("Profesor creado con éxito");
                    break;
                case 6://GUARDAR PROFESOR
                    pd.altaProfesor(p);
                    System.out.println("Profesor guardado con éxito");
                    break;
                case 7://LEER PROFESOR
                    p=pd.obtenerProfesor(1);
                    System.out.println(p);
                    break;
                case 8://ELIMINAR PROFESOR
                    pd.bajaProfesor(p);
                    System.out.println("Profesor eliminado con éxito");
                    break;
                case 9://SALIR
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 9);

    }

    public static void menu() {
        System.out.println("CONSIDERACIÓN 1. EJECUTAR LOS PASOS POR ORDEN");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR ALUMNO, COMO SON FIJOS DARÁ ERROR");
        System.out.println("1. Crear alumno");
        System.out.println("2. Guardar alumno");
        System.out.println("3. Leer alumno");
        System.out.println("4. Eliminar alumno");
        System.out.println("5. Crear profesor");
        System.out.println("6. Guardar profesor");
        System.out.println("7. Leer profesor");
        System.out.println("8. Eliminar profesor");
        System.out.println("9. Salir");
    }
}
