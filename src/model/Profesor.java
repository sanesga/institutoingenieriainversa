package model;

public class Profesor  implements java.io.Serializable {


     private Integer id;
     private TipoFuncionario tipoFuncionario;
     private Nombre nombre;

    public Profesor() {
    }

    public Profesor(Nombre nombre,TipoFuncionario tipoFuncionario) {
        
        this.nombre = nombre;
        this.tipoFuncionario = tipoFuncionario;
    }

    @Override
    public String toString() {
        return "Profesor{" + "id=" + id + ", tipoFuncionario=" + tipoFuncionario + ", nombre=" + nombre.getNombreCompleto()+ '}';
    }
    
    

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public TipoFuncionario getTipoFuncionario() {
        return tipoFuncionario;
    }

    public void setTipoFuncionario(TipoFuncionario tipoFuncionario) {
        this.tipoFuncionario = tipoFuncionario;
    }

    public Nombre getNombre() {
        return nombre;
    }

    public void setNombre(Nombre nombre) {
        this.nombre = nombre;
    }
}


